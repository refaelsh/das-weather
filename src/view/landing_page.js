import * as ko from "knockout";
import "./landing_page.css";

import { MainViewModel } from "../view_model/main_view_model.js";

const mainViewModel = new MainViewModel();
ko.applyBindings(mainViewModel);

async function f() {
  const t0 = performance.now();
  const response = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=London&APPID=28235ae73743908080d4b836a89d0e14`, { mode: `cors` });
  const weatherJson = await response.json();
  const t1 = performance.now();
  console.log(`Call to doSomething took ` + (t1 - t0) + ` milliseconds.`);
  console.log(weatherJson);
}

f();
console.log(4);
