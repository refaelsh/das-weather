const path = require(`path`);
const HtmlWebpackPlugin = require(`html-webpack-plugin`);
const MiniCssExtractPlugin = require(`mini-css-extract-plugin`);

module.exports = {
  mode: `development`,

  // entry: [`./src/index.js`],
  entry: [`./src/view/landing_page.js`],

  devtool: `inline-source-map`,

  devServer: {
    contentBase: `./dist`
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: `Custom template`,
      template: `./src/view/landing_page.html`,
      favicon: `./src/view/favicon.png`
    }),
    new MiniCssExtractPlugin()
  ],

  output: {
    filename: `main.js`,
    path: path.resolve(__dirname, `dist`),
    clean: true
  },

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          `style-loader`,
          `css-loader`
        ]
      },
      {
        test: /\.(html)$/,
        use: [`html-loader`]
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: `asset/resource`
      }
    ]
  }
};
